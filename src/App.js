import React from "react";
import { Switch, Route } from "react-router-dom";

import { Home, Edit, Create } from "./Views/index";

const App = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/create">
        <Create />
      </Route>
      <Route exact path="/edit/:id">
        <Edit />
      </Route>
    </Switch>
  );
};

export default App;
