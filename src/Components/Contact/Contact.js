import React from "react";
import styles from "./styles.module.css";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers";

import TextInput from "../../Components/TextInput";

const ContactSchema = yup.object().shape({
  firstName: yup.string().required(),
  lastName: yup.string().required(),
  email: yup.string().email().required(),
  phone: yup.string().required(),
});

export default function Contact({ info, onSubmit }) {
  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(ContactSchema),
  });
  return (
    <form
      onSubmit={handleSubmit(onSubmit)}
      style={{
        display: "flex",
        flexDirection: "column",
        maxWidth: "610px",
        alignItems: "center",
      }}
    >
      <div className={styles.inputContainer}>
        <span>First Name: </span>
        <TextInput
          placeholder="First name"
          name="firstName"
          innerRef={register}
          defaultValue={info && info.first_name}
          className={styles.textInput}
        />
        {errors.firstName && (
          <span className={styles.error}>{errors.firstName.message}</span>
        )}
      </div>{" "}
      <div className={styles.inputContainer}>
        <span>Last Name: </span>
        <TextInput
          placeholder="Last name"
          name="lastName"
          innerRef={register}
          defaultValue={info && info.last_name}
          className={styles.textInput}
        />
        {errors.lastName && (
          <span className={styles.error}>{errors.lastName.message}</span>
        )}
      </div>
      <div className={styles.inputContainer}>
        <span>Email: </span>
        <TextInput
          placeholder="Email"
          name="email"
          innerRef={register}
          defaultValue={info && info.email}
          className={styles.textInput}
        />
        {errors.email && (
          <span className={styles.error}>{errors.email.message}</span>
        )}
      </div>
      <div className={styles.inputContainer}>
        <span>Phone: </span>
        <TextInput
          placeholder="Phone"
          name="phone"
          innerRef={register}
          defaultValue={info && info.phone}
          className={styles.textInput}
        />
        {errors.phone && (
          <span className={styles.error}>{errors.phone.message}</span>
        )}
      </div>
      <button type="submit">{info ? "Edit" : "Add"} contact</button>
    </form>
  );
}
