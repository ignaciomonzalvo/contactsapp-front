import React from "react";
import styles from "./styles.module.css";

export default function ContactRow({
  contact: { id, first_name, last_name, email, phone },
  deleteContact,
  goToEdit,
}) {
  return (
    <li className={styles.container}>
      <span
        style={{ fontWeight: "500", fontSize: "24px", marginBottom: "8px" }}
        key={`post-${id}`}
      >{`${first_name} ${last_name}`}</span>
      <div style={{ marginLeft: "16px" }}>
        <span style={{ fontWeight: "500" }}>Email: </span> {email}
      </div>
      <div style={{ marginLeft: "16px" }}>
        <span style={{ fontWeight: "500" }}>Phone: </span> {phone}
      </div>
      <div className={styles.optionsContainer}>
        <button
          onClick={goToEdit}
          className={styles.cursorPointer}
          style={{ marginRight: "8px" }}
        >
          Edit
        </button>
        <button
          onClick={() => deleteContact(id)}
          className={styles.cursorPointer}
          style={{ color: "red" }}
        >
          Delete
        </button>
      </div>
    </li>
  );
}
