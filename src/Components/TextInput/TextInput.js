import React from "react";

import styles from "./styles.module.css";

export default function TextInput({
  className,
  onChange,
  value,
  disabled,
  innerRef,
  ...rest
}) {
  return (
    <input
      className={`${styles.input} ${className}`}
      value={value}
      onChange={onChange}
      disabled={disabled}
      type="text"
      ref={innerRef}
      {...rest}
    />
  );
}
