import React from "react";
import axios from "axios";
import styles from "./styles.module.css";
import { useParams, useHistory } from "react-router-dom";
import Contact from "../../Components/Contact";

const createUser = async ({ firstName, lastName, email, phone }) => {
  await axios({
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    data: {
      first_name: firstName,
      last_name: lastName,
      phone: phone,
      email: email,
    },
    method: "post",
    url: `${process.env.REACT_APP_API_URL}/contacts`,
  });
};
export default function Create() {
  let history = useHistory();

  const onSubmit = async (data) => {
    await createUser(data);
    history.push("/");
  };

  return (
    <div className={styles.mainLayoutMain}>
      <h1>Create Contact</h1>
      <Contact onSubmit={onSubmit} />
    </div>
  );
}
