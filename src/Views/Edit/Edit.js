import React from "react";
import axios from "axios";
import styles from "./styles.module.css";
import { useQuery, useMutation, queryCache } from "react-query";
import { useParams, useHistory } from "react-router-dom";
import Contact from "../../Components/Contact";

const getContact = async (id) => {
  const response = await axios({
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    method: "get",
    url: `${process.env.REACT_APP_API_URL}/contacts/${id}`,
  });

  return response.data;
};

export default function Edit() {
  let { id } = useParams();
  let history = useHistory();
  const { status, data, isFetching, error } = useQuery(
    ["contact", { id }],
    () => getContact(id)
  );

  const onSubmit = async ({ firstName, lastName, email, phone }) => {
    const response = await axios({
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      data: {
        first_name: firstName,
        last_name: lastName,
        phone: phone,
        email: email,
      },
      method: "put",
      url: `${process.env.REACT_APP_API_URL}/contacts/${id}`,
    });
    queryCache.invalidateQueries("contacts");
    history.push("/");
    return response;
  };

  const [mutateUpdateContact] = useMutation(onSubmit, {
    onSuccess: (data, updatedContact) => {
      queryCache.setQueryData(["contact", { id }], {
        ...updatedContact,
        first_name: updatedContact.firstName,
        last_name: updatedContact.lastName,
      });
    },
  });

  return (
    <div className={styles.mainLayoutMain}>
      <h1>Edit Contact</h1>
      {data && <Contact info={data} onSubmit={mutateUpdateContact} />}
    </div>
  );
}
