import React from "react";
import axios from "axios";
import styles from "./styles.module.css";
import { useHistory } from "react-router-dom";
import { useQuery, useMutation, queryCache } from "react-query";

import ContactRow from "../../Components/ContactRow";

const getContacts = async () => {
  const response = await axios({
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    method: "get",
    url: `${process.env.REACT_APP_API_URL}/contacts`,
  });

  return response.data.contacts;
};

const deleteContact = (id) =>
  axios.delete(`${process.env.REACT_APP_API_URL}/contacts/${id}`, {
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
  });

export default function Home() {
  let history = useHistory();
  const { status, data, isFetching, error } = useQuery("contacts", getContacts);

  const [mutateDeleteContact] = useMutation(deleteContact, {
    onSuccess: (data, id) => {
      queryCache.setQueryData(
        "contacts",
        queryCache.getQueryData("contacts").filter((item) => {
          return item.id !== id;
        })
      );
    },
  });

  if (status === "loading") {
    return <div>loading...</div>;
  }

  if (status === "error") {
    return <div>{error.message}</div>;
  }

  return (
    <div className={styles.mainLayoutMain}>
      <h1 style={{ margin: 0 }}>Contacts App</h1>
      <button
        onClick={() => history.push("/create")}
        style={{ marginLeft: "48px", marginTop: "16px" }}
      >
        Create new user
      </button>
      {data && (
        <ul className={styles.contactsContainer}>
          {data &&
            data.map((contact) => (
              <ContactRow
                key={contact.id}
                contact={contact}
                deleteContact={mutateDeleteContact}
                goToEdit={() => history.push(`/edit/${contact.id}`)}
              />
            ))}
        </ul>
      )}
      {isFetching && <p>updating...</p>}
    </div>
  );
}
